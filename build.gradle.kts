import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.3.11"
}

group = "com.protocase"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("org.yaml", "snakeyaml", "1.23")
    testCompile("org.junit.jupiter", "junit-jupiter-api", "5.4.0")
    testCompile("org.junit.jupiter", "junit-jupiter-params", "5.4.0")
    testCompile("org.junit.jupiter", "junit-jupiter-engine", "5.4.0")
    testCompile("io.mockk","mockk", "1.8.13.kotlin13")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

val jar by tasks.getting(Jar::class) {
    archiveName="jbrains.jar"
}

tasks {
    named<Test>("test") {
        useJUnitPlatform()
    }
}
