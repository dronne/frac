package com.protocase.jbrains.jars;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class JarClassloader {
    private File jarFile = null;

    public JarClassloader(File jarFile) {
        this.jarFile = jarFile;
    }

    public Class clazz(String qualifiedClazz) throws IOException, ClassNotFoundException {
        Class classToLoad;
        try (URLClassLoader child = new URLClassLoader(
                new URL[]{jarFile.toURI().toURL()},
                this.getClass().getClassLoader()
        )) {
            classToLoad = Class.forName(qualifiedClazz, true, child);
        }
        return classToLoad;
    }
}

