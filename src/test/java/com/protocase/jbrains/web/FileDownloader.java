package com.protocase.jbrains.web;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class FileDownloader {

    public void download(String urlString, File outputFile) throws MalformedURLException {
        URL url = new URL(urlString);
        InputStream is = null;
        OutputStream os = null;
        try {
            os = new FileOutputStream(new File("..."));
            is = url.openStream();
            // Read bytes...
        } catch (
                IOException exp) {
            exp.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception exp) {
            }
            try {
                os.close();
            } catch (Exception exp) {
            }
        }
    }
}
