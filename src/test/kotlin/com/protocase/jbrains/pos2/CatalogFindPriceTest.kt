package com.protocase.jbrains.pos2

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

abstract class CatalogFindPriceTest {
    @Test
    fun productNotFound() {

        assertNull(catalogWithout("::product not found::")
                        .findPrice("::product not in catalog::"))
    }

    @Test
    fun productFound() {

        val priceOfProduct = Price.cents(1250)
        assertEquals(
            priceOfProduct,
            catalogWith("::product in catalog::", priceOfProduct).findPrice("::product in catalog::"))
    }

    protected abstract fun catalogWithout(barcode: String): Catalog

    protected abstract fun catalogWith(barcode: String, price: Price): InMemoryCatalog
}