package com.protocase.jbrains.pos2

internal class InMemoryCatalogFindPriceTest : CatalogFindPriceTest() {

    override fun catalogWithout(barcode: String): Catalog = InMemoryCatalog(mapOf(Pair("Anything but $barcode", Price.cents(0))))

    override fun catalogWith(barcode: String, price: Price) =
        InMemoryCatalog(mapOf(
            Pair("::definitely not $barcode::", Price.cents(3)),
            Pair(barcode, price),
            Pair("::another that is definitely not $barcode::", Price.cents(2))))
}

class InMemoryCatalog(private val pricesForBarcodes: Map<String, Price>) : Catalog {

    override fun findPrice(barcode: String): Price? {
        return pricesForBarcodes[barcode]
    }

}
