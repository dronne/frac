package com.protocase.jbrains.pos2

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PriceTest {

    fun centsToFormattedArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(795, "$7.95"),
            Arguments.of(2, "$0.02"),
            Arguments.of(100, "$1.00"),
            Arguments.of(101, "$1.01"),
            Arguments.of(100000, "$1,000.00"),
            Arguments.of(999999999, "$9,999,999.99")

        )
    }

    @ParameterizedTest(name = "Price Format for {0} should be {1}")
    @MethodSource("centsToFormattedArguments")
    fun priceToString(cents: Int, formatted: String) {
        assertEquals(formatted, Price.cents(cents).toString())
    }
}