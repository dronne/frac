package com.protocase.jbrains.pos2

import io.mockk.*
import org.junit.jupiter.api.Test


internal class SellOneItemControllerTest {


    private fun mockCatalogAndDisplay(): Pair<Catalog, Display> {
        val mockCatalog = mockk<Catalog>()
        val mockDisplay = mockk<Display>()
        return Pair(mockCatalog, mockDisplay)
    }

    @Test
    fun oneItemFound() {
        val irrelevantPrice = Price.cents(795)

        val (mockCatalog, mockDisplay) = mockCatalogAndDisplay()


        every { mockCatalog.findPrice("::product in catalog::") } returns irrelevantPrice
        every { mockDisplay.displayPrice(irrelevantPrice)} just Runs

        SaleController(mockCatalog, mockDisplay).onBarcode("::product in catalog::")

        verify (exactly = 1){
            mockDisplay.displayPrice(irrelevantPrice)
        }
    }

    @Test
    fun oneItemNotFound() {
        val (mockCatalog, mockDisplay) = mockCatalogAndDisplay()

        every { mockCatalog.findPrice("::product not in catalog::") } returns null
        every { mockDisplay.displayProductNotFound("::product not in catalog::")} just Runs

        SaleController(mockCatalog, mockDisplay).onBarcode("::product not in catalog::")

        verify (exactly = 1) {
            mockDisplay.displayProductNotFound("::product not in catalog::")
        }
    }

    @Test
    fun emptyBarcode() {
        val (mockCatalog, mockDisplay) = mockCatalogAndDisplay()

        val emptyBarcode = ""

        every { mockDisplay.displayEmptyBarcode()} just Runs

        SaleController(mockCatalog, mockDisplay).onBarcode(emptyBarcode)

        verify (exactly = 0) {
            mockDisplay.displayPrice(any())
            mockDisplay.displayProductNotFound(any())
        }

        verify (exactly = 1) {
            mockDisplay.displayEmptyBarcode()
        }
    }
}

interface Display {
    fun displayPrice(price: Price)
    fun displayProductNotFound(barcode: String)
    fun displayEmptyBarcode()
}

class SaleController(val catalog: Catalog, val display: Display) {
    fun onBarcode(barcode: String) {
        if (barcode.isBlank())
            display.displayEmptyBarcode()
        else
            catalog.findPrice(barcode)?.let {
                display.displayPrice(it)
            } ?: display.displayProductNotFound(barcode)
    }
}


