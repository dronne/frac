package com.protocase.jbrains.pos2

import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ConsoleDisplayTest {
    var sout: PrintStream? = null
    var encoding = Charsets.UTF_8

    @BeforeAll
    fun setDefaultEncoding() {
        encoding = Charset.forName(System.getProperty("file.encoding"))
    }

    @BeforeEach
    fun rememberSystemOut() {
        sout = System.out
    }

    @AfterEach
    fun restoreSystemOut() {
        sout?.let{System.setOut(it)}
    }


    private fun replaceSoutWithCanvas(): ByteArrayOutputStream {
        val canvas = ByteArrayOutputStream()
        System.setOut(PrintStream(canvas))
        return canvas
    }

    @Test
    fun testProductNotFound() {
        println("Encoding: ${encoding.toString()}")
        val canvas = replaceSoutWithCanvas()

        val display = ConsoleDisplay()
        display.displayProductNotFound("::barcode::")

        assertEquals(listOf("Product not found: ::barcode::"), canvas.toString(encoding.toString()).lines().filter{it.isNotBlank()})
    }

    @Test
    fun emptyBarcode() {
        val canvas = replaceSoutWithCanvas()

        val display = ConsoleDisplay()
        display.displayEmptyBarcode()

        assertEquals(listOf("Scan error: Empty barcode"), canvas.toString(encoding.toString()).lines().filterNot { it.isBlank() })
    }

    @Test
    fun displayPrice() {
        val canvas = replaceSoutWithCanvas()

        val display = ConsoleDisplay()
        val anyPrice = Price.cents(795)
        display.displayPrice(anyPrice)

        assertEquals(listOf(anyPrice.toString()), canvas.toString(encoding.toString()).lines().filterNot { it.isBlank() })
    }

    @Test
    fun displayMultipleMessages() {
        val canvas = replaceSoutWithCanvas()

        val display = ConsoleDisplay()
        val anyPrice = Price.cents(795)
        val anyOtherPrice = Price.cents(120001)

        display.displayPrice(anyPrice)
        display.displayEmptyBarcode()
        display.displayPrice(anyOtherPrice)
        display.displayProductNotFound("::barcode not found::")

        assertEquals(
            listOf(
                anyPrice.toString(),
                "Scan error: Empty barcode",
                anyOtherPrice.toString(),
                "Product not found: ::barcode not found::"
            ),
            canvas.toString(encoding.toString()).lines().filterNot { it.isBlank() })
    }
}

class ConsoleDisplay : Display{
    private val PRICE_DISPLAY_TEMPLATE = "%s"

    override fun displayPrice(price: Price) {
        render(PRICE_DISPLAY_TEMPLATE, price.toString())
    }

    private fun render(template: String, vararg templateValues: String) {
        println(formatMessageTemplate(template, templateValues))
    }

    private fun formatMessageTemplate(
        template: String,
        templateValues: Array<out String>
    ) = template.format(*templateValues)

    override fun displayProductNotFound(barcode: String) {
        render("Product not found: %s", barcode)
    }

    override fun displayEmptyBarcode() {
        render("Scan error: Empty barcode")

    }
}
