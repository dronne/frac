package com.protocase.jbrains.pos2

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class TestHowToDoConsoleHijackingTest {
    var sout: PrintStream? = null

    @BeforeEach
    fun rememberSystemOut() {
        sout = System.out
    }

    @AfterEach
    fun restoreSystemOut() {
        sout?.let{System.setOut(it)}
    }

    @Test
    fun testHijack() {
        println("hi")

        val canvas = ByteArrayOutputStream()
        System.setOut(PrintStream(canvas))


        println("boo!")
        assertEquals(listOf("boo!"), canvas.toString("utf-8").lines().filterNot { it.isBlank() })
    }
}