package com.protocase.jbrains

import com.protocase.jbrains.jars.JarClassloader
import com.protocase.jbrains.web.FileDownloader
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.File

class PackageTest {

    @Test
    fun testPackagedJarWorks() {
        val tmpJar = File.createTempFile("frac-", ".jar")
        tmpJar.deleteOnExit()
        
        val url = "https://www.gitlab.com/dronne/frac/-/jobs/artifacts/master/raw/build/lib/jbrains.jar?job=build"
        FileDownloader().download(url, tmpJar)

        val clazz = JarClassloader(tmpJar).clazz("com.protocase.jbrains.math.Frac")
        val expected = clazz.getConstructor(Int::class.java, Int::class.java).newInstance(41,15)
        val f1 = clazz.getConstructor(Int::class.java, Int::class.java).newInstance(4,3)
        val f2 = clazz.getConstructor(Int::class.java, Int::class.java).newInstance(7,5)
        val method = clazz.getMethod("plus", clazz)
        assertEquals(expected, method.invoke(f1, f2))
    }
}