package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialSubtractionTest {

    fun provideArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(0), Poly(0), Poly(0)),
            Arguments.of(Poly(5), Poly(5), Poly(0)),
            Arguments.of(Poly(-17), Poly(0), Poly(17)),
            Arguments.of(Poly(6), Poly(0), Poly(-6)),
            Arguments.of(Poly(12), Poly(6), Poly(-6)),
            Arguments.of(Poly(1, 3), Poly(1, 6), Poly(3)),
            Arguments.of(Poly(-4, 6, -1, 1), Poly(-4, 0, 0, 4), Poly(-6, 1, 3))
        )
    }

    @ParameterizedTest
    @MethodSource("provideArguments")
    fun testSubtraction(expected: Poly, p1: Poly, p2: Poly) {
        assertEquals(expected, p1 - p2)
    }
}