package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialToStringTest {

    fun provideTestPairs() : Stream<Arguments> {
        return Stream.of(
            Arguments.of("1", Poly(1)),
            Arguments.of("-7", Poly(-7)),
            Arguments.of("3x^2 + 4x + 1", Poly(3, 4, 1)),
            Arguments.of("2x^2 + 17", Poly(2, 0, 17)),
            Arguments.of("14x^3 - 17", Poly(14, 0, 0, -17)),
            Arguments.of("12x - 3", Poly(0, 0, 12, -3))
            )
    }

    @ParameterizedTest
    @MethodSource("provideTestPairs")
    fun testOne(string: String, polynomial: Poly) {
        assertEquals(string, polynomial.toString())
    }
}