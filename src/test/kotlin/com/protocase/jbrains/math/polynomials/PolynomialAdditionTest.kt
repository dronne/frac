package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialAdditionTest {

    fun providesArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(0), Poly(0), Poly(0)),
            Arguments.of(Poly(3), Poly(0), Poly(3)),
            Arguments.of(Poly(-1), Poly(2), Poly(-3)),
            Arguments.of(Poly(1, 4, -4), Poly(4, -7), Poly(1, 0, 3))
        )
    }

    @ParameterizedTest
    @MethodSource("providesArguments")
    fun testAddition(expected: Poly, p1: Poly, p2: Poly) {
        assertEquals(expected, p1 + p2)
    }
}