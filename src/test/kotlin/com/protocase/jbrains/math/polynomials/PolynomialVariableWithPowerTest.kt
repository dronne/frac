package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialVariableWithPowerTest {

    fun provideArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(1), 0, ""),
            Arguments.of(Poly(1,2,3), 0, "x^2"),
            Arguments.of(Poly(1,2,3), 1, "x"),
            Arguments.of(Poly(1,2,3), 2, "")
        )
    }

    @ParameterizedTest
    @MethodSource("provideArguments")
    fun testVariableWithPower(polynomial: Poly, index: Int, variableWithPower: String) {
        assertEquals(variableWithPower, polynomial.variableWithPower(index))
    }
}