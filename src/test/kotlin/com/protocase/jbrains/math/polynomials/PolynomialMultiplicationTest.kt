package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialMultiplicationTest {

    fun provideArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(0), Poly(0), Poly(0)),
            Arguments.of(Poly(0), Poly(0), Poly(5)),
            Arguments.of(Poly(0), Poly(-3), Poly(0)),
            Arguments.of(Poly(-15), Poly(3), Poly(-5)),
            Arguments.of(Poly(6, -8), Poly(2), Poly(3, -4)),
            Arguments.of(Poly(3, 2, -8), Poly(1, 2), Poly(3, -4)),
            Arguments.of(Poly(-9, 15, 2, -8), Poly(-3, 1, 2), Poly(3, -4))

        )
    }

    @ParameterizedTest
    @MethodSource("provideArguments")
    fun testMultiplication(expected: Poly, p1: Poly,  p2: Poly) {
        assertEquals(expected, p1 * p2)
    }

    @Test
    fun testCoefficients() {
        println((3 downTo 0).flatMap {t -> (2 downTo 0).map { o -> Pair(t, o) } }.toList())
        println((0 downTo 0).toList())
    }
}