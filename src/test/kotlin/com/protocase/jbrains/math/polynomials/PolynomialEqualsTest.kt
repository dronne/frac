package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialEqualsTest {

    fun provideArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(1), Poly(1), true),
            Arguments.of(Poly(3, 0, 2), Poly(3, 0, 2), true),
            Arguments.of(Poly(3, 0, 2), Poly(3, 0, 1), false),
            Arguments.of(Poly(-17, 14, -3), Poly(-17, 14, -3), true),
            Arguments.of(Poly(1, 2), Poly(2, 2), false),
            Arguments.of(Poly(0, 0, 1, 2), Poly(1, 2), true),
            Arguments.of(Poly(3, 0, 0), Poly(Pair(3, 2)), true),
            Arguments.of(Poly(3), Poly(Pair(3, 0)), true)

        )
    }

    @ParameterizedTest
    @MethodSource("provideArguments")
    fun testEquals(p1: Poly, p2: Poly, shouldEqual: Boolean) {
        assertEquals(shouldEqual, p1.equals(p2), "$p1 and $p2 ${if (shouldEqual) "should equal" else "should not equal"}")
    }

}