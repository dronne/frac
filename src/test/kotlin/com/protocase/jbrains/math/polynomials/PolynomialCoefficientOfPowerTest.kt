package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialCoefficientOfPowerTest {

    fun provideArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(1), 0, 1),
            Arguments.of(Poly(-3),0, -3),
            Arguments.of(Poly(4), 1, 0),
            Arguments.of(Poly(17, 4), 1, 17),
            Arguments.of(Poly(17, 4), 0, 4)
        )
    }

    @ParameterizedTest
    @MethodSource("provideArguments")
    fun testCoefficientOfPower(poly: Poly, power: Int, coefficient: Int) {
        assertEquals(coefficient, poly.coefficientOfPower(power))
    }
}