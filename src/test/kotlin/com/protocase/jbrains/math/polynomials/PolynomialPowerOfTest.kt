package com.protocase.jbrains.math.polynomials

import com.protocase.jbrains.math.Poly
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PolynomialPowerOfTest {

    fun provideArguments() : Stream<Arguments> {
        return Stream.of(
            Arguments.of(Poly(1), 0, 0),
            Arguments.of(Poly(1,2,3), 0, 2),
            Arguments.of(Poly(1,2,3), 1, 1),
            Arguments.of(Poly(1,2,3), 2, 0)
        )
    }

    @ParameterizedTest(name = "{0} has index power: {1}")
    @MethodSource("provideArguments")
    fun testPowerOf(polynomial: Poly, index: Int, power: Int) {
        assertEquals(power, polynomial.powerOf(index))
    }
}