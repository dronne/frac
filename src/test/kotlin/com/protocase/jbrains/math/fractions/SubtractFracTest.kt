package com.protocase.jbrains.math.fractions

import com.protocase.jbrains.math.Frac
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SubtractFracTest {

    @Test
    fun testSubtraction() {
        assertEquals(
            Frac(5, 4), Frac(
                3,
                2
            ) - Frac(1, 4)
        )
    }

    @Test
    fun test5Minus7() {
        assertEquals(
            Frac(-2), Frac(5) - Frac(
                7
            )
        )
    }

    @Test
    fun testCommutableNegAddition() {
        assertEquals(
            Frac(1) - Frac(2), Frac(
                1
            ) + Frac(-2)
        )
    }


    @Test
    fun testMinusWhole() {
        assertEquals(Frac(-13, 3), Frac(2, 3) - 5)
    }


}