package com.protocase.jbrains.math.fractions

import com.protocase.jbrains.math.Frac
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

class DivideFracTest {



    @Test
    fun testDivision() {
        assertEquals(
            Frac(2, 5), Frac(
                -3,
                2
            ) / Frac(15, -4)
        )
    }

    @Test
    fun testDivideByZero() {
        val thrown = assertThrows(ArithmeticException::class.java) {
            Frac(1) / Frac(0)
        }

        assertEquals("Divide By Zero!", thrown.message)
    }


    @Test
    fun testDivWhole() {
        assertEquals(Frac(3, 8), Frac(3, 2) / 4)
    }

    @Test
    fun testIndeterminantResult() {
        val thrown = assertThrows(ArithmeticException::class.java) {
            Frac(0) / Frac(0)
        }

        assertEquals("Zero Divided By Zero is Indeterminate!", thrown.message)
    }

}