package com.protocase.jbrains.math.fractions

import com.protocase.jbrains.math.Frac
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class FracEqualsTest {

    @Test
    fun testSimpleEquals() {
        assertEquals(Frac(3, 17), Frac(3, 17))
    }

    @Test
    fun testDifferentNumerators() {
        assertNotEquals(Frac(2, 3), Frac(1, 3))
    }

    @Test
    fun testDifferentDenominators() {
        assertNotEquals(Frac(7, 12), Frac(7, 15))
    }

    @Test
    fun testWholeNumberNotEqualsOtherWholeNumber() {
        assertNotEquals(Frac(3), Frac(9))
    }

    @Test
    fun testToString() {
        assertEquals("3/2", Frac(-6, -4).toString())
    }

    @Test
    fun test0ToString() {
        assertEquals("0/1", Frac(0, 2).toString())
    }

    @Test
    fun testNegativesEquals() {
        assertEquals(Frac(-14, 2), Frac(7, -1))
        assertEquals(Frac(-14, 2), Frac(7, -1))
    }

    @Test
    fun testNegativeNotEquals() {
        assertNotEquals(Frac(1, 2), Frac(-1, 2))

    }

    @Test
    fun testFracOfWholeNumbers() {
        assertEquals(Frac(2), Frac(2, 1))
    }

    @Test
    fun testWholeNumbers() {
        assertEquals(Frac(-45), -45)
    }

    @Test
    fun testOtherZeros() {
        assertEquals(Frac(0, 2), Frac(0, 3))
    }

    @Test
    fun testCommutableNeg() {
        assertEquals(Frac(-7, 1), Frac(7, -1))
    }

    @Test
    fun testReduction() {
        assertEquals(Frac(7, 2), Frac(42, 12))
    }

    @Test
    fun testNull() {
        assertFalse(Frac(1).equals(null))
    }

}