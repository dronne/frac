package com.protocase.jbrains.math.fractions

import com.protocase.jbrains.math.Frac
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class AddFracTest {


    @Test
    fun testZeros() {
        assertEquals(
            Frac(0), Frac(0) + Frac(
                0
            )
        )
    }

    @Test
    fun test0Plus1() {
        assertEquals(
            Frac(1), Frac(0) + Frac(
                1
            )
        )
    }

    @Test
    fun testAddition() {
        assertEquals(
            Frac(41, 15), Frac(
                4,
                3
            ) + Frac(7, 5)
        )
    }

    @Test
    fun testMoreNegatives() {
        assertEquals(
            Frac(1, 2), Frac(
                1,
                -4
            ) + Frac(-3, -4)
        )
    }


    @Test
    fun testSummWhole() {
        assertEquals(Frac(1, 2), Frac(-1, 2) + 1)
    }
}