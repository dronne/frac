package com.protocase.jbrains.math.fractions

import com.protocase.jbrains.math.Frac
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MultiplyFracTest {

    @Test
    fun testMultiply() {
        assertEquals(
            Frac(4, 5), Frac(
                2,
                3
            ) * Frac(6, 5)
        )
    }

    @Test
    fun testTimesWhole() {
        assertEquals(Frac(-35, 6), Frac(-7, 12) * 10)
    }
}