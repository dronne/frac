package com.protocase.jbrains.math.fractions

import com.protocase.jbrains.math.Frac
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

class FracTest {


    @Test
    fun test1Div0() {
        val thrown = assertThrows(ArithmeticException::class.java) { Frac(1, 0) }

        assertEquals("Zero Denominator!", thrown.message)
    }


    @Test
    fun testReducedString() {
        assertEquals("5/4", Frac(15, 12).toString())
    }


    @Test
    fun testIndeterminant() {
        val thrown = assertThrows(ArithmeticException::class.java) {
            Frac(0, 0)
        }

        assertEquals("Zero Divided By Zero is Indeterminate!", thrown.message)
    }

}