package com.protocase.jbrains.math

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class FactorerTest {

    @Test
    fun findNextFac0() {
        assertNull(Factorer.findNextFactors(0))
    }

    @Test
    fun findNextFac1() {
        assertNull(Factorer.findNextFactors(7))
    }

    @Test
    fun findNextFactor12() {
        assertEquals(Pair(2, 6), Factorer.findNextFactors(12))
    }

    @Test
    fun factor() {
        assertEquals(listOf(2, 2, 3, 7), Factorer.factor(84))
    }

    @Test
    fun factorNegative() {
        assertEquals(emptyList<Int>(), Factorer.factor(-4))
    }

    @Test
    fun factorZero() {
        assertEquals(listOf<Int>(), Factorer.factor(0))
    }


    @Test
    fun factorOne() {
        assertEquals(listOf(1), Factorer.factor(1))
    }


    @Test
    fun isEven() {
        (-11..11).forEach {
            if (it % 2 == 0)
                assertTrue(Factorer.isEven(it))
            else
                assertFalse(Factorer.isEven(it))
        }
    }
}