package com.protocase.jbrains.pos

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ShoppingCartTotalTest {

    fun totalsToTest() : Stream<Arguments> = Stream.of(
        Arguments.of("An Empty list gives zero total", emptyList<Int?>(), null),
        Arguments.of("A Single Item gives the item as total", listOf(795), 795),
        Arguments.of("A Single zero Item gives zero as total", listOf(0), 0),
        Arguments.of("A Single null item gives null total", listOf(null), null),
        Arguments.of("several non-null items give total", listOf(1, 11, 111, 100, 0), 223),
        Arguments.of("any null items give null total", listOf(1, null, 111, 100, 0), null),
        Arguments.of("first item null items give null total", listOf(null, 111, 100, 0), null)
    )

    @ParameterizedTest(name = "{0}")
    @MethodSource("totalsToTest")
    fun testTotals(testName: String, saleItemsInCents: List<Int>, totalCents: Int?) {
        println(testName)

        assertEquals(totalCents, ShoppingCart.calculateTotal(saleItemsInCents))
    }

}