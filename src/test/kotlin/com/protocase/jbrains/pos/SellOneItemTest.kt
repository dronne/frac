package com.protocase.jbrains.pos

import com.protocase.jbrains.pos.POSCreator.createPointOfSaleWithTestCatalog
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SellOneItemTest {

    @Test
    fun oneExample() {
        val display = InternalDisplay()
        createPointOfSaleWithTestCatalog(display)
            .onBarcode("12345")
        assertEquals("$7.95", display.text())
    }

    @Test
    fun anotherExample() {
        val display = InternalDisplay();
        createPointOfSaleWithTestCatalog(display).onBarcode("12346")
        assertEquals("$12.44", display.text())
    }

    @Test
    fun aThirdExample() {
        val display = InternalDisplay()
        createPointOfSaleWithTestCatalog(display).onBarcode("543210")
        assertEquals("$5.50", display.text())
    }

    @Test
    fun aMissingBarcode() {
        val display = InternalDisplay()
        createPointOfSaleWithTestCatalog(display).onBarcode("55555")
        assertEquals("Scan error: Barcode 55555 not found", display.text())
    }

    @Test
    fun anEmptyBarcode() {
        val display = InternalDisplay()
        createPointOfSaleWithTestCatalog(display).onBarcode("")
        assertEquals("Scan error: Barcode is empty", display.text())
    }
}
