package com.protocase.jbrains.pos

import com.protocase.jbrains.pos.POSCreator.createPointOfSaleWithTestCatalog
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class SellMultipleItemsTest {

    @Test
    fun testNoItemsScannedGivesWarning() {
        val display = InternalDisplay()
        val pos = createPointOfSaleWithTestCatalog(display)

        pos.onTotal()

        assertEquals("No sale in progress, scan a barcode", display.text())
    }

    @Test
    fun oneItem_found() {
        val display = InternalDisplay()
        val pos = createPointOfSaleWithTestCatalog(display)

        pos.onBarcode("12345")
        pos.onTotal()

        assertEquals("Total: $7.95", display.text())
    }

    @Test
    fun oneItem_notFound() {
        val display = InternalDisplay()
        val pos = createPointOfSaleWithTestCatalog(display)

        pos.onBarcode("22345")
        pos.onTotal()

        assertEquals("No sale in progress, scan a barcode", display.text())
    }

    @Test
    fun manyItems_found() {
        val display = InternalDisplay()
        val pos = createPointOfSaleWithTestCatalog(display)

        pos.onBarcode("12345")
        pos.onBarcode("12346")
        pos.onBarcode("543210")
        pos.onTotal()

        assertEquals("Total: $25.89", display.text())
    }

    @Test
    fun manyItems_OneNotFound() {
        val display = InternalDisplay()
        val pos = createPointOfSaleWithTestCatalog(display)

        pos.onBarcode("an item that you won't find in barcodes")
        pos.onBarcode("12346")
        pos.onBarcode("543210")
        pos.onTotal()

        assertEquals("No sale in progress, scan a barcode", display.text())

    }
}