package com.protocase.jbrains.pos

object POSCreator {

    fun createPointOfSaleWithTestCatalog(display: IDisplay) : PointOfSale {
        return PointOfSale(
            display,
            PricesForBarCodes(
                PricesFromBarcodesYamlLoader(
                    PricesForBarCodes::class.java.getResourceAsStream("/PricesForBarCodes.yml")
                ).loadPricesByBarcode()
            )
        )
    }
}