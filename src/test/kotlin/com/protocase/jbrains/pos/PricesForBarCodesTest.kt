package com.protocase.jbrains.pos

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class PricesForBarCodesTest {

    private fun createPricesForBarCodes() : Map<String, Any> {
        return PricesFromBarcodesYamlLoader(
            PricesForBarCodes::class.java.getResourceAsStream("/PricesForBarCodes.yml")
        ).loadPricesByBarcode()
    }

    @Test
    fun oneExample() {
        assertEquals(795, createPricesForBarCodes()["12345"])
    }

    @Test
    fun anotherExample() {
        assertEquals(1244, createPricesForBarCodes()["12346"])
    }

    @Test
    fun aThirdExample() {
        assertEquals(550, createPricesForBarCodes()["543210"])    }

    @Test
    fun aMissingBarcode() {
        assertNull(createPricesForBarCodes()["55555"])
    }
}