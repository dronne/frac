package com.protocase.jbrains.pos

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PriceFormattingTest {

    fun expectedFormattedPricesForCents() = Stream.of(
        Arguments.of(432, "$4.32"),
        Arguments.of(2, "$0.02"),
        Arguments.of(5000, "$50.00"),
        Arguments.of(120000, "$1,200.00"),
        Arguments.of(100120000, "$1,001,200.00"),
        Arguments.of(99999999, "$999,999.99"),
        Arguments.of(0, "$0.00")

    )

    @ParameterizedTest
    @MethodSource("expectedFormattedPricesForCents")
    fun simple(cents: Int, formattedPrice: String) {
        assertEquals(formattedPrice, InternalDisplay().formatPrice(cents))
    }
}