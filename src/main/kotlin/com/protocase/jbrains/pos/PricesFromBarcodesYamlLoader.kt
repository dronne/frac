package com.protocase.jbrains.pos

import org.yaml.snakeyaml.Yaml
import java.io.InputStream

class PricesFromBarcodesYamlLoader(private val pricesByBarcodInputStream: InputStream) {

    fun createPricesForBarCodes() =
        PricesForBarCodes(
            loadPricesByBarcode()
        )

    fun loadPricesByBarcode(): Map<String, Int> {
        return Yaml().load<Map<String, Map<String, Int>>>(pricesByBarcodInputStream)
            ?.let {
                if (it.containsKey("PricesForBarCodes"))
                    it["PricesForBarCodes"] ?: emptyMap()
                else
                    emptyMap()

            } ?: emptyMap()
    }
}