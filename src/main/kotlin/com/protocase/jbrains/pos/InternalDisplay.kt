package com.protocase.jbrains.pos

class InternalDisplay : IDisplay {
    private var _text: String = ""

    override fun text(): String {
        return this._text
    }

    override fun setPriceFromCents(cents: Int) { this._text = formatPrice(cents) }

    override fun scanError(error: String) { this._text = "Scan error: $error" }

    override fun setTotalFromCents(totalCents: Int) { this._text = "Total: ${formatPrice(totalCents)}" }

    override fun setEmptyTotal() { this._text = "No sale in progress, scan a barcode" }

    internal fun formatPrice(price: Int) : String { return "$%,d.%02d".format(price/100, price%100) }
}