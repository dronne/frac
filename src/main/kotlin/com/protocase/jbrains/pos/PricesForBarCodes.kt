package com.protocase.jbrains.pos

class PricesForBarCodes(
    private val pricesForBarcodes: Map<String, Int>
) {

    fun hasBarCode(barCode: String) = pricesForBarcodes.containsKey(barCode)

    operator fun get(barCode: String) = pricesForBarcodes[barCode]
}