package com.protocase.jbrains.pos

object ShoppingCart {
    fun calculateTotal(saleItemsInCents: List<Int?>) : Int? {
        return when {
            saleItemsInCents.any {it == null} -> null
            saleItemsInCents.isEmpty() -> null
            else -> saleItemsInCents.fold(0){ acc, itemInCents -> itemInCents?.let {  acc + it } ?: -1 }
        }
    }
}