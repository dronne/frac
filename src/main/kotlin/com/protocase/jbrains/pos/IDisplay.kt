package com.protocase.jbrains.pos

interface IDisplay {
    fun text() : String
    fun setPriceFromCents(cents: Int)
    fun scanError(error: String)
    fun setTotalFromCents(totalCents: Int)
    fun setEmptyTotal()
}