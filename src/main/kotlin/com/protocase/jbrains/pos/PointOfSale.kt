package com.protocase.jbrains.pos

class PointOfSale(internal val display: IDisplay,
                  private val pricesForBarCodes: PricesForBarCodes) {

    private var scannedPrices = emptyList<Int?>()

    fun onBarcode(barcode: String) {
        if (barcode.isEmpty()) {
            display.scanError("Barcode is empty")
            return
        }

        if ( ! pricesForBarCodes.hasBarCode(barcode)) {
            display.scanError("Barcode $barcode not found")
            this.scannedPrices += listOf<Int?>(null)
            return
        }

        val priceForThisItem = pricesForBarCodes[barcode]

        priceForThisItem?.let {
            display.setPriceFromCents(it)
        } ?: display.scanError("Barcode $barcode had null scannedPrice")

        this.scannedPrices += priceForThisItem
    }

    fun onTotal() {

        ShoppingCart.calculateTotal(scannedPrices)
            ?.let {
                display.setTotalFromCents(it)
            } ?: display.setEmptyTotal()

        resetSale()
    }

    private fun resetSale() {
        scannedPrices = emptyList()
    }

    fun calculateTotalForSale(scannedPrices: List<Int>) = scannedPrices.firstOrNull()
}