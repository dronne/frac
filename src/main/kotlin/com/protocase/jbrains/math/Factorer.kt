package com.protocase.jbrains.math

object Factorer {
    fun factor(num: Int) : MutableList<Int> {
        if (num <= 0)
            return mutableListOf<Int>()

        if (num == 1)
            return mutableListOf(1)

        var n = num
        val result = mutableListOf<Int>()
        while (isEven(n)) {
            n = n shr 1
            result += 2
        }

        var nextFac = findNextFactors(n)
        while (nextFac != null) {
            result += nextFac.first
            n = nextFac.second
            nextFac = findNextFactors(n)
        }
        result += n

        return result
    }

    fun findNextFactors(num: Int) : Pair<Int, Int>? {
        return (2..num/2).asSequence()
                         .filter { num % it == 0 }
                         .map { Pair(it, num/it) }
                         .firstOrNull()
    }

    fun isEven(num: Int) : Boolean {
        return (num and 1) == 0
    }


}