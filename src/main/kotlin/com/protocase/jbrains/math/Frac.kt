package com.protocase.jbrains.math

import kotlin.math.abs

class Frac(private val numerator: Int, private val denominator: Int) {
    constructor(numerator: Int) : this(numerator, 1)

    private val reducedFrac: Pair<Int, Int>
    init {
        reducedFrac = reduce()
    }

    private fun reduce() : Pair<Int, Int> {
        if (denominator == 0) {
            if (numerator == 0)
                throw ArithmeticException("Zero Divided By Zero is Indeterminate!")

            throw ArithmeticException("Zero Denominator!")
        }
        if (numerator == 0)
            return Pair(0,1)

        val neg = (numerator < 0) xor (denominator < 0)
        var num = abs(numerator)
        var den = abs(denominator)

        val numFacs = Factorer.factor(num)
        val denFacs = Factorer.factor(den)

        var factor = getNextCommonFac(numFacs, denFacs)
        while (factor != null) {
            numFacs.remove(factor)
            denFacs.remove(factor)
            factor = getNextCommonFac(numFacs, denFacs)
        }
        num = numFacs.fold(1) { a, b -> a*b}

        if (neg)
            num *= -1

        den = denFacs.fold(1) { a, b -> a*b}
        return Pair(num, den)
    }


    private fun getNextCommonFac(l1: MutableList<Int>, l2: MutableList<Int>) : Int? {
        return l1.asSequence()
                 .firstOrNull { l2.contains(it) }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        val that =
            (if (other is Int)
                Frac(other)
            else if (other is Frac)
                other
            else
                null) ?: return false

        if (reducedFrac.first != that.reducedFrac.first) return false
        if (reducedFrac.second != that.reducedFrac.second) return false

        return true
    }

    override fun hashCode(): Int {
        var result = reducedFrac.first
        result = 31 * result + reducedFrac.second
        return result
    }

    override fun toString(): String {
        return "${reducedFrac.first}/${reducedFrac.second}"
    }

    operator fun plus(other: Frac) : Frac {
        return Frac(other.denominator * numerator + other.numerator * denominator, denominator * other.denominator)
    }

    operator fun minus(other: Frac) : Frac {
        return Frac(other.denominator * numerator - other.numerator * denominator, denominator * other.denominator)
    }

    operator fun times(other: Frac) : Frac {
        return Frac(numerator * other.numerator, denominator * other.denominator)
    }

    operator fun div(other: Frac) : Frac {
        if(numerator != 0 && other.numerator == 0) {
            throw ArithmeticException("Divide By Zero!")
        }
        return Frac(numerator * other.denominator, denominator * other.numerator)
    }

    operator fun plus(other: Int) : Frac {
        return this + Frac(other)
    }

    operator fun minus(other: Int) : Frac {
        return this - Frac(other)
    }

    operator fun div(other: Int) : Frac {
        return this / Frac(other)
    }

    operator fun times(other: Int) : Frac {
        return this * Frac(other)
    }

    operator fun Int.plus(other: Frac) : Frac {
        return Frac(this) + other
    }
}