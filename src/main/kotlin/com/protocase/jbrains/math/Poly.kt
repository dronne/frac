package com.protocase.jbrains.math

import kotlin.math.abs
import kotlin.math.max

class Poly(internal vararg val coefficients: Int) {
    constructor(coefPower: Pair<Int, Int>) : this(*IntArray(coefPower.second+1){ if (it == 0) coefPower.first else 0})

    private val trimmedCoefficients: List<Int>


    init {
        val indexNonZero = (0 until coefficients.size)
            .firstOrNull { coefficients[it] != 0 } ?: coefficients.size - 1

        trimmedCoefficients = coefficients.slice(indexNonZero until coefficients.size)
    }

    operator fun times(other: Any?) : Poly {
        if (! (other is Poly))
            throw ArithmeticException("Trying to subtract non-Polynomial from Polynomial!")

        if (this.degree() > 0) {
            return (this.degree() downTo 0)
                .flatMap {t -> (other.degree() downTo 0).map { o -> Pair(t, o) } }
                .map { Poly(Pair(this.coefficientOfPower(it.first) * other.coefficientOfPower(it.second), it.first + it.second)) }
                .foldRight(Poly(0)){p1, p2 -> p1 + p2}
        }
        return Poly(*other.trimmedCoefficients.map { it * this.trimmedCoefficients[0] }.toIntArray())
    }

    operator fun minus(other: Any?) : Poly {
        if (! (other is Poly))
            throw ArithmeticException("Trying to subtract non-Polynomial from Polynomial!")

        val maxDegree = max(this.degree(), other.degree())

        return Poly(
            *(maxDegree+1 downTo 0)
                .map { this.coefficientOfPower(it) - other.coefficientOfPower(it) }
                .toIntArray())
    }

    operator fun plus(other: Any?): Poly {
        if (! (other is Poly))
            throw ArithmeticException("Trying to add non-Polynomial to Polynomial!")

        val maxDegree = max(this.degree(), other.degree())

        return Poly(
            *(maxDegree+1 downTo 0)
                .map { this.coefficientOfPower(it) + other.coefficientOfPower(it) }
                .toIntArray())
    }

    override fun toString(): String {
        return (1 until trimmedCoefficients.size)
                .filter { trimmedCoefficients[it] != 0 }
                .map {
                    if (trimmedCoefficients[it] < 0)
                        " - ${abs(trimmedCoefficients[it])}${variableWithPower(it)}"
                    else
                        " + ${trimmedCoefficients[it]}${variableWithPower(it)}"
                }
                .fold("${trimmedCoefficients[0]}${variableWithPower(0)}"){ a, b -> "$a$b"}
    }

    fun degree() = trimmedCoefficients.size - 1

    fun coefficientOfPower(power: Int): Int {
        if(power > trimmedCoefficients.size-1)
            return 0
        return trimmedCoefficients[degree() - power]
    }

    internal fun variableWithPower(index: Int) : String {
        val power = powerOf(index)

        return when (power) {
            0 -> ""
            1 -> "x"
            else -> "x^$power"
        }
    }

    internal fun powerOf(index: Int) : Int {
        return trimmedCoefficients.size - index - 1
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Poly

        if (!trimmedCoefficients.equals(other.trimmedCoefficients)) return false

        return true
    }

    override fun hashCode(): Int {
        return trimmedCoefficients.hashCode()
    }
}
