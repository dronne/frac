package com.protocase.jbrains.pos2

interface Catalog {
    fun findPrice(barcode: String): Price?
}