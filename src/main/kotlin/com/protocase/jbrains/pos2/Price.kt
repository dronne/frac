package com.protocase.jbrains.pos2

data class Price(val cents: Int) {
    companion object {
        fun cents(cents: Int) = Price(cents)
    }

    override fun toString(): String {
        return "$%,d.%02d".format(cents/100, cents % 100)
    }
}